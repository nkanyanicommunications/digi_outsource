﻿using test4.Models;
using System.Configuration;
using System.Messaging;
using System.Data.SqlClient;
using System.Web.Mvc;
using System;

namespace test4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PlayersModel player)
        {
            const string queueName = @".\private$\TestQueue";
            MessageQueue msMq = msMq = new MessageQueue(queueName);

            msMq.Formatter = new XmlMessageFormatter(new Type[] { typeof(PlayersModel) });

            var message = (PlayersModel)msMq.Receive().Body;

            string constr = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                string query = "INSERT INTO players(name, surname, email, date) VALUES(@Name, @Surname, @Email, @date)";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Name", message.Name);
                    cmd.Parameters.AddWithValue("@Surname", message.Surname);
                    cmd.Parameters.AddWithValue("@Email", message.Email);
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return View(player);
        }
    }
}