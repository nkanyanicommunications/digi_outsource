﻿using System;

namespace test2
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write("Enter the number of rows: ");
            string input = System.Console.ReadLine();

            int number = Convert.ToInt32(input);


            for (int y = 0; y < number; y++)
            {
                int c = 1;
                for (int q = 0; q < number - y; q++)
                {
                    System.Console.Write("   ");
                }

                for (int x = 0; x <= y; x++)
                {
                    System.Console.Write("   {0:D} ", c);
                    c = c * (y - x) / (x + 1);
                }
                System.Console.WriteLine();
                System.Console.WriteLine();
            }
            System.Console.WriteLine();

        }
    }
    
}
