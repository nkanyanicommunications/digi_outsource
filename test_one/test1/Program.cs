﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace test1
{
    class Program
    {
        public static List<int> listToSort = new List<int>();
        public static String line;
        static void Main(string[] args)
        {
            
            try
            {
                ReadFile();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        static void ReadFile()
        {
            StreamReader sr = new StreamReader("C:\\digi_outsource\\test_one\\test1\\input\\readtext.txt");

            line = sr.ReadLine();
            while (line != null)
            {
                listToSort.Add(SplitAndSumFunction(line));
                line = sr.ReadLine();
            }

            var sortedList = listToSort.OrderBy(m => m).ToList();
            Console.WriteLine("output result:");
            foreach (var item in sortedList)
            {

                Console.WriteLine(item);
                line = sr.ReadLine();
            }
            WriteToFile(sortedList);
            sr.Close();
            Console.ReadLine();
        }

        static void WriteToFile(List<int> output)
        {
            string fileName = "C:\\digi_outsource\\test_one\\test1\\output\\writetext.txt";

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using (StreamWriter sw = File.CreateText(fileName))
            {
                foreach (var item in output)
                {
                    sw.WriteLine(item);
                }
            }
        }

        static int SplitAndSumFunction(string input)
        {
            var answer = 0;
            var strArray = input.Split(',').ToList();
            foreach (var str in strArray)
            {
                answer += Convert.ToInt32(str);
            }
            return answer;
        }
    }
}
