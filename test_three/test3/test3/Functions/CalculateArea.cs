﻿using System;
using test3.DataTransferObjects;

namespace test3.Calculate
{
    public class Functions
    {
        public double CalculateArea(BoxType _boxType)
        {
            if (_boxType == BoxType.Rectangle)
            {
                return AreaRequest.Width * AreaRequest.Height;
            }
            else if (_boxType == BoxType.Square)
            {
                return AreaRequest.Width * AreaRequest.Height;
            }
            else if (_boxType == BoxType.Circle)
            {
                return AreaRequest.Radius * Math.PI;
            }
            else
            {
                return 0;
            }
        }   
    }

}
