﻿using test3.DataTransferObjects;
using System.Data.SqlClient;

namespace test3.Functions
{
    public class Functions
    {
        public void Add(BoxType _boxType)
        {
            using (var con = new SqlConnection())
            {
                SqlCommand cmd;
                if (_boxType == BoxType.Circle)
                {
                    cmd = new SqlCommand(string.Format("insert into [Table] values({ 0 }, { 1})", _boxType, DataTransferObjects.AreaRequest.Radius), con);
                }
                else
                {
                    cmd = new SqlCommand(string.Format("insert into [Table] values({0}, {1}, {2})", _boxType, DataTransferObjects.AreaRequest.Width, DataTransferObjects.AreaRequest.Height), con);
                }
                cmd.ExecuteNonQuery();
            }
        }
    }
}
