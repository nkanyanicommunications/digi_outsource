﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test3.DataTransferObjects
{
    public static class AreaRequest
    {
        public static double Width { get; set; }
        public static double Height { get; set; }
        public static double Radius { get; set; }
    }
}
