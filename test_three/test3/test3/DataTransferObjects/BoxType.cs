﻿
namespace test3.DataTransferObjects
{
    public enum BoxType
    {
        Unknown,
        Square,
        Rectangle,
        Circle
    }

}
