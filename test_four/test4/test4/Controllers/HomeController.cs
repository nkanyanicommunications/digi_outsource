﻿using test4.Models;
using System.Messaging;

using System.Web.Mvc;

namespace test4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PlayersModel player)
        {
            const string queueName = @".\private$\TestQueue";
            MessageQueue msMq = null;

            if (!MessageQueue.Exists(queueName))
            {
                msMq = MessageQueue.Create(queueName);
            }
            else
            {
                msMq = new MessageQueue(queueName);
            }
            PlayersModel p = new PlayersModel()
            {
                Name = player.Name,
                Surname = player.Surname,
                Email = player.Email
            };
            msMq.Send(p);
 

            return View(player);
        }
    }
}